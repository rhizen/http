package middleware

import "net/http"

func Post(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Method == "POST" {
			handler.ServeHTTP(w, req)
		} else {
			w.Header().Set("Allow", "POST")
			if req.Method == "OPTIONS" {
				w.WriteHeader(http.StatusOK)
			} else {
				http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			}
		}
	})
}

func Get(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Method == "GET" {
			handler.ServeHTTP(w, req)
		} else {
			w.Header().Set("Allow", "GET")
			if req.Method == "OPTIONS" {
				w.WriteHeader(http.StatusOK)
			} else {
				http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			}
		}
	})
}
