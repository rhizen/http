package middleware

import (
	"net/http"
	"reflect"

	"pkg.cocoad.mobi/x/log"
)

func GetArg(paramName, fieldName string) func(http.Handler) http.Handler {
	return getArg(false, paramName, fieldName)
}

func MustGetArg(paramName, fieldName string) func(http.Handler) http.Handler {
	return getArg(true, paramName, fieldName)
}

func getArg(must bool, paramName, fieldName string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			arg := req.URL.Query().Get(paramName)
			if arg == "" {
				if must {
					log.BadRequest(w, req).Warnf("%s must not be empty", paramName)
					return
				}
			} else if err := setFieldByName(req, fieldName, arg); err != nil {
				if must {
					log.ServerError(w, req).Errorf(err.Error())
					return
				}
			}
			next.ServeHTTP(w, req)
		})
	}
}
func ParseArg(paramName string, parser interface{}, fieldNames ...string) func(http.Handler) http.Handler {
	return parseArg(false, paramName, parser, fieldNames)
}

func MustParseArg(paramName string, parser interface{}, fieldNames ...string) func(http.Handler) http.Handler {
	return parseArg(true, paramName, parser, fieldNames)
}

func parseArg(must bool, paramName string, parser interface{}, fieldNames []string) func(http.Handler) http.Handler {
	t := reflect.TypeOf(parser)
	if t.Kind() != reflect.Func ||
		t.NumIn() != 1 ||
		t.In(0).Kind() != reflect.String ||
		t.NumOut() != len(fieldNames)+1 {
		panic("parser should be func(string) (result1, result2, ..., error), and the count of results should be equal to len(fieldNames)")
	}
	f := reflect.ValueOf(parser)
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			rs := f.Call([]reflect.Value{reflect.ValueOf(req.URL.Query().Get(paramName))})
			if ev := rs[len(rs)-1]; !ev.IsNil() {
				if must {
					log.BadRequest(w, req).Warn(ev.Interface().(error).Error())
					return
				}
			} else {
				for i := 0; i < len(rs)-1; i++ {
					if err := setFieldByName(req, fieldNames[i], rs[i].Interface()); err != nil {
						if must {
							log.BadRequest(w, req).Warn(err.Error())
							return
						}
					}
				}
			}
			next.ServeHTTP(w, req)
		})
	}
}
