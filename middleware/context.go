package middleware

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"

	"github.com/gorilla/context"
)

func SetVar(structValue interface{}) func(http.Handler) http.Handler {
	t := reflect.TypeOf(structValue)
	if t.Kind() != reflect.Struct {
		panic("a variable of struct type should be passed in")
	}
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
			defer context.Clear(req)
			context.Set(req, "v", reflect.New(t).Interface())
			next.ServeHTTP(w, req)
		})
	}
}

func GetVar(req *http.Request) interface{} {
	return context.Get(req, "v")
}

func setFieldByName(req *http.Request, fieldName string, value interface{}) error {
	s := GetVar(req)
	if s == nil {
		return errors.New("func Var should be put as the first middleware")
	}
	field := reflect.ValueOf(s).Elem().FieldByName(fieldName)
	if !field.CanSet() {
		return fmt.Errorf("field %s cannot be set", fieldName)
	}
	field.Set(reflect.ValueOf(value))
	return nil
}
