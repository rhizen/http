package middleware

import (
	"net/http"

	"pkg.cocoad.mobi/x/log"
)

func Ping(w http.ResponseWriter, req *http.Request) {
	w.Write([]byte("OK"))
	log.Fields{
		"request": req.RequestURI,
		"ip":      GetRealIP(req),
	}.Debug("ping OK")
}
