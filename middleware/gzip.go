package middleware

import (
	"compress/gzip"
	"io"
	"net/http"
)

type gzipReadCloser struct {
	rc io.ReadCloser
	*gzip.Reader
}

func newGzipReadCloser(rc io.ReadCloser) (*gzipReadCloser, error) {
	reader, err := gzip.NewReader(rc)
	if err != nil {
		return nil, err
	}
	return &gzipReadCloser{
		rc:     rc,
		Reader: reader,
	}, nil
}

func (r *gzipReadCloser) Close() error {
	if err := r.rc.Close(); err != nil {
		return err
	}
	return r.Reader.Close()
}

type gzipResponseWriter struct {
	http.ResponseWriter
	w *gzip.Writer
}

func newGzipResponseWriter(rw http.ResponseWriter) *gzipResponseWriter {
	return &gzipResponseWriter{ResponseWriter: rw, w: gzip.NewWriter(rw)}
}
func (gw gzipResponseWriter) Write(b []byte) (int, error) { return gw.w.Write(b) }
func (gw gzipResponseWriter) close() error                { return gw.w.Close() }

// Gzip decode the request body with header("Content-Encoding":"gzip")
func Gzip(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.Header.Get("Content-Encoding") == "gzip" {
			rc, err := newGzipReadCloser(req.Body)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
			req.Body = rc
		}
		if req.Header.Get("Accept-Encoding") == "gzip" {
			w.Header().Set("Content-Encoding", "gzip")
			gw := newGzipResponseWriter(w)
			func() {
				defer gw.close()
				handler.ServeHTTP(gw, req)
			}()
		} else {
			handler.ServeHTTP(w, req)
		}
	})
}
