package middleware

import (
	"net/http"
	"runtime"

	"pkg.cocoad.mobi/x/log"
)

func Recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				stack := make([]byte, 1024)
				stack = stack[:runtime.Stack(stack, false)]
				log.Fields{
					"error": err,
					"stack": stack,
				}.Error("panic")
			}
		}()
		next.ServeHTTP(w, r)
	})
}
