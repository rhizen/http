package middleware

import (
	"net"
	"net/http"
	"strings"

	"pkg.cocoad.mobi/x/log"
)

func RealIP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		ip := GetRealIP(req)
		if err := setFieldByName(req, "IP", ip); err != nil {
			log.ServerError(w, req).Errorf(err.Error())
		}
		next.ServeHTTP(w, req)
	})
}

func GetRealIP(req *http.Request) string {
	ip, _, _ := net.SplitHostPort(req.RemoteAddr)
	if realIP := req.Header.Get("X-Real-IP"); realIP != "" {
		ip = realIP
	} else if forwarded := ForwardedFor(req); forwarded != "" {
		ip = forwarded
	}

	return ip
}

func ForwardedFor(r *http.Request) string {
	if addr := r.Header.Get("X-Forwarded-For"); addr != "" {
		p := strings.Split(addr, ",")
		addr = strings.Trim(p[len(p)-1], " ")
		ip := strings.Split(addr, ":")
		return ip[0]
	}

	return ""
}
