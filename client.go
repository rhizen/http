package xhttp

import (
	"math"
	"net"
	"net/http"
	"time"
)

type RetryableFunc func(*http.Request, *http.Response, error) bool
type WaitFunc func(try int)
type DeadlineFunc func() time.Time

type ResilientTransport struct {
	// Timeout is the maximum amount of time a dial will wait for
	// a connect to complete.
	//
	// The default is no timeout.
	//
	// With or without a timeout, the operating system may impose
	// its own earlier timeout. For instance, TCP timeouts are
	// often around 3 minutes.
	DialTimeout time.Duration

	// MaxTries, if non-zero, specifies the number of times we will retry on
	// failure. Retries are only attempted for temporary network errors or known
	// safe failures.
	MaxTries    int
	Deadline    DeadlineFunc
	ShouldRetry RetryableFunc
	Wait        WaitFunc
	transport   *http.Transport
}

func NewTrasport() *ResilientTransport {
	return &ResilientTransport{
		Deadline: func() time.Time {
			return time.Now().Add(15 * time.Second)
		},
		DialTimeout: 30 * time.Second,
		MaxTries:    3,
		ShouldRetry: shouldRetry,
		Wait:        expBackoff,
	}
}

func NewClient(rt *ResilientTransport) *http.Client {
	rt.transport = &http.Transport{
		Dial: func(netw, addr string) (net.Conn, error) {
			c, err := net.DialTimeout(netw, addr, rt.DialTimeout)
			if err != nil {
				return nil, err
			}
			c.SetDeadline(rt.Deadline())
			return c, nil
		},
		DisableKeepAlives:   true,
		Proxy:               http.ProxyFromEnvironment,
		TLSHandshakeTimeout: 15 * time.Second,
	}
	// TODO: Would be nice is ResilientTransport allowed clients to initialize
	// with http.Transport attributes.
	return &http.Client{
		Timeout:   10 * time.Second,
		Transport: rt,
	}
}

func (r *ResilientTransport) RoundTrip(req *http.Request) (resp *http.Response, err error) {
	for try := 0; try < r.MaxTries; try++ {
		resp, err = r.transport.RoundTrip(req)

		if !r.ShouldRetry(req, resp, err) {
			break
		}
		if resp != nil {
			resp.Body.Close()
		}
		if r.Wait != nil {
			r.Wait(try)
		}
	}

	return
}

func shouldRetry(req *http.Request, res *http.Response, err error) bool {
	retry := false

	// Retry if there's a temporary network error.
	if neterr, ok := err.(net.Error); ok {
		if neterr.Temporary() {
			retry = true
		}
	}

	// Retry if we get a 5xx series error.
	if res != nil {
		if res.StatusCode >= 500 && res.StatusCode < 600 {
			retry = true
		}
	}

	return retry
}

func expBackoff(try int) {
	time.Sleep(200 * time.Millisecond * time.Duration(math.Exp2(float64(try))))
}

func LinearBackoff(try int) {
	time.Sleep(time.Duration(try*200) * time.Millisecond)
}
