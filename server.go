package xhttp

import (
  "fmt"
  "net"
  "net/http"

  "github.com/pkg/errors"
  "gopkg.in/tylerb/graceful.v1"
  "pkg.cocoad.mobi/x/http/middleware"
  "pkg.cocoad.mobi/x/log"
)

type HTTPServer struct {
  l net.Listener
  s *graceful.Server
  *http.ServeMux
}

func NewHTTPServer(addr string) (*HTTPServer, error) {
  l, err := net.Listen("tcp", addr)
  if err != nil {
    return nil, errors.Wrap(err, fmt.Sprintf("listen on %s fail", addr))
  }

  mux := http.NewServeMux()
  s := &HTTPServer{
    l:	l,
    ServeMux: mux,
    s: &graceful.Server{
      Server:	&http.Server{
        Handler: mux,
      },
    },
  }

  s.HandleFunc("/ping", middleware.Ping)
  return s, nil
}

func (s *HTTPServer) ServeHTTP() error {
  log.Fields{
    "addr": s.l.Addr().String(),
  }.Info("HTTP server is listening")
  return s.s.Serve(s.l)
}
