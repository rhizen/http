package tests

import (
  "pkg.cocoad.mobi/x/http" //package name is xhttp for the conflicts against to net/http
  "net/http/httptest"
  "net/http"
  "testing"
  "io/ioutil"
  "fmt"
)

func TestClientHttpGET(t *testing.T) {
  // echoHandler, passes back form parameter p
  echoHandler := func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprint(w, r.FormValue("p"))
  }
  // create test server with handler
  ts := httptest.NewServer(http.HandlerFunc(echoHandler))
  defer ts.Close()

  // here we call our xhttp client to test using httptest server.
  client := xhttp.NewClient(xhttp.NewTrasport())
  res, err := client.Get(ts.URL + "/?p=hello")
  if err != nil {
    t.Errorf("Error: %v", err)
    return
  }
  defer res.Body.Close()

  result, err := ioutil.ReadAll(res.Body)
  if err != nil {
    t.Errorf("Error: %v", err)
  }
  // confirm result
  if res.StatusCode != http.StatusOK {
    t.Errorf("Unexpected status code: %v", res.StatusCode)
  }
  if string(result) != "hello" {
    t.Errorf("Unexpected result: %v", result)
  }
}
