package tests

import (
  "testing"
  "net/http"
  "pkg.cocoad.mobi/x/http/middleware"
)

func TestRealIp(t *testing.T) {
	req := &http.Request{Header: http.Header{"X-Forwarded-For": []string{"ip1, ip2, client-ip"}}}

	ip := middleware.ForwardedFor(req)
	if ip != "client-ip" {
	  t.Errorf("Expected %s, Got %s", "client-ip", ip)
  }
}
